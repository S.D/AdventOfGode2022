package main

import (
	"bufio"
	"log"
	"os"

	"git.jacknet.io/S.D/AdventOfGode2022/level1"
	"git.jacknet.io/S.D/AdventOfGode2022/level2"
)

func readFile(s string) []string {
	f, err := os.Open(s)
	if err != nil {
		log.Fatal(err)
		return make([]string, 0)
	}

	defer f.Close()

	// read the file line by line using scanner
	scanner := bufio.NewScanner(f)

	fileData := make([]string, 0, 100)

	for scanner.Scan() {
		newString := scanner.Text()
		fileData = append(fileData, newString)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
		return make([]string, 0)
	}

	return fileData
}

func runLevelOne() {
	fileInput := readFile("input/input1")
	intGroups := level1.GetIntGroups(fileInput)
	biggest := level1.GetNBiggest(intGroups, 1)
	log.Println(biggest)
	biggest = level1.GetNBiggest(intGroups, 3)
	log.Println(biggest)
	log.Println(level1.SumGroup(biggest))
}

func runLevelTwo() {
	fileInput := readFile("input/input2")
	level2Parsed := level2.ParseInput(fileInput)
	log.Println(level2.TotalScore(level2Parsed))
	log.Println(level2.TotalScore2(level2Parsed))
}

func main() {
	log.Println("Hello World!")
	runLevelOne()
	runLevelTwo()
}
