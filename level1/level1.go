package level1

import (
	"log"
	"sort"
	"strconv"
	"strings"
)

func SumGroup(in []int) int {
	result := 0
	for _, data := range in {
		result += data
	}
	return result
}

func GetBiggestIntGroup(in [][]int) int {
	max := 0
	for _, data := range in {
		total := SumGroup(data)
		if total > max {
			max = total
		}
	}
	return max
}

func GetNBiggest(in [][]int, num int) []int {
	output := make([]int, 0, len(in))
	for _, data := range in {
		output = append(output, SumGroup(data))
	}
	sort.Slice(output, func(x, y int) bool {
		return output[x] > output[y]
	})
	return output[:num]
}

func GetIntGroups(in []string) [][]int {
	output := make([][]int, 0, 20)
	group := make([]int, 0, 10)

	for _, data := range in {
		if strings.TrimSpace(data) == "" {
			output = append(output, group)
			group = make([]int, 0, 10)
		} else {
			conv, err := strconv.ParseInt(data, 10, 0)
			if err != nil {
				log.Fatal(err)
				continue
			}
			group = append(group, int(conv))
		}
	}
	return output
}
