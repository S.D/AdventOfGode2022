package level1

import (
	"testing"
)

func TestSumGroup(t *testing.T) {
	input := []int{2, 5, 4, 3}
	result := SumGroup(input[:])
	if result != 14 {
		t.Log("Invalid Result, got ", result)
		t.Fail()
	}
}

func TestBiggestSumGroup(t *testing.T) {
	input := [][]int{{1, 2}, {3, 4}, {1, 2}, {2, 2}}
	result := GetBiggestIntGroup(input[:])
	if result != 7 {
		t.Log("Invalid Result, got ", result)
		t.Fail()
	}
}
