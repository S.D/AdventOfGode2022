package level2

import (
	"log"
	"strings"
)

const (
	Lose     = 0
	Draw     = 3
	Win      = 6
	Rock     = 1
	Paper    = 2
	Scissors = 3
)

func calcScore(in []string) int {
	x, y := in[0], in[1]
	switch {
	case x == "A" && y == "X":
		return Draw + Rock
	case x == "A" && y == "Y":
		return Win + Paper
	case x == "A" && y == "Z":
		return Lose + Scissors
	case x == "B" && y == "X":
		return Lose + Rock
	case x == "B" && y == "Y":
		return Draw + Paper
	case x == "B" && y == "Z":
		return Win + Scissors
	case x == "C" && y == "X":
		return Win + Rock
	case x == "C" && y == "Y":
		return Lose + Paper
	case x == "C" && y == "Z":
		return Draw + Scissors
	default:
		log.Panic("Invalid input")
		return -1
	}
}

func calcScore2(in []string) int {
	x, y := in[0], in[1]
	switch {
	case x == "A" && y == "X":
		return Lose + Scissors
	case x == "A" && y == "Y":
		return Draw + Rock
	case x == "A" && y == "Z":
		return Win + Paper
	case x == "B" && y == "X":
		return Lose + Rock
	case x == "B" && y == "Y":
		return Draw + Paper
	case x == "B" && y == "Z":
		return Win + Scissors
	case x == "C" && y == "X":
		return Lose + Paper
	case x == "C" && y == "Y":
		return Draw + Scissors
	case x == "C" && y == "Z":
		return Win + Rock
	default:
		log.Panic("Invalid input")
		return -1
	}
}

func TotalScore(in [][]string) int {
	total := 0
	for _, data := range in {
		total += calcScore(data)
	}
	return total
}

func TotalScore2(in [][]string) int {
	total := 0
	for _, data := range in {
		total += calcScore2(data)
	}
	return total
}

func ParseInput(in []string) [][]string {
	output := make([][]string, 0, len(in))
	for _, data := range in {
		components := strings.Fields(data)
		if len(components) != 2 {
			log.Panic("Invalid string input\n")
			return output
		}
		output = append(output, components)
	}
	return output
}
